import org.apache.commons.io.*;


import org.eclipse.jgit.api.DescribeCommand.*;
import org.eclipse.jgit.api.*;
import org.eclipse.jgit.lib.*;
import org.eclipse.jgit.revwalk.*;
import org.eclipse.jgit.api.errors.*;
import org.eclipse.jgit.errors.*;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.treewalk.*;
import org.eclipse.jgit.treewalk.filter.PathFilter;
import org.eclipse.jgit.blame.BlameResult;
import org.eclipse.jgit.api.ListBranchCommand.ListMode;
import org.eclipse.jgit.diff.*;
import org.eclipse.jgit.util.io.DisabledOutputStream;
import org.eclipse.jgit.patch.FileHeader;
import org.eclipse.jgit.patch.HunkHeader;
import org.eclipse.jgit.api.DescribeCommand.*;
import mypackages.*;

import java.lang.Boolean;
import java.io.File;
import java.util.List;
import java.util.ArrayList;
import java.io.IOException;
import java.text.ParseException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;
import java.text.DecimalFormat;
import java.util.Date;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import java.text.SimpleDateFormat;

class Main{
	public static void main(String[] args) throws IOException, GitAPIException, IllegalStateException {
		System.out.println(">> Producing the report. Please wait... <<");

		String pathRepo, pathReports;
		int countCommits = 0, countFiles = 0;
		Iterable<RevCommit> commits;
		List<String> authorList = new ArrayList<String>();
		Map<String, Integer> filesToSize = new HashMap<String, Integer>();
		List<String> branchList = new ArrayList<String>();
		ArrayList<String> templist = new ArrayList<String>();
		Map<String, Double> authorToCommits = new HashMap<String, Double>();
		Map<String, Integer> branchesToCommits = new HashMap<String, Integer>();
		Map<String, Integer> authorToLines = new HashMap<String, Integer>();
		Map<String, ArrayList<String>> authorToBranches = new HashMap<String, ArrayList<String>>();
		Map<String, ArrayList<Double>> authorToAvgDMY = new HashMap<String, ArrayList<Double>>();
		HashMap<String, HashMap<String, Integer>> authorToBranchesToCommits = new HashMap<String, HashMap<String, Integer>>();
		String authorName;
		int countTags = 0, countBranches = 0;

		pathRepo = args[0] + "/.git";
		pathReports = args[1];

		//1) counts the commits and saves their ids in var commits
		// Open an existing repository
		Repository existingRepo = new FileRepositoryBuilder()
		    .setGitDir(new File(pathRepo))
		    .build();

		try (Git git = new Git(existingRepo)) {
	        commits = git.log().all().call();

	        for (RevCommit commit : commits) {
	            //System.out.println("LogCommit: " + commit);
	            countCommits++;
	            authorName = commit.getAuthorIdent().getName();
	            //System.out.println("author: " + authorName);
	            if(!authorList.contains(authorName)){
	            	authorList.add(authorName);
	            	authorToCommits.put(authorName, 1.0d);
	            } else{
					authorToCommits.put(authorName, authorToCommits.get(authorName) + 1.0d);
	            }
            }
        }
//		System.out.println("Number of overall commits in repository: " + countCommits);
//		System.out.println("Number of authors: " + authorList.size());
        

        //2) getting the files in repo
        Ref head = existingRepo.findRef("HEAD");
        int currentLines = 0;
        // a RevWalk allows to walk over commits based on some filtering that is defined
        try (RevWalk walk = new RevWalk(existingRepo)) {
            RevCommit commit = walk.parseCommit(head.getObjectId());
            RevTree tree = commit.getTree();
            //System.out.println("Having tree: " + tree);

            // now use a TreeWalk to iterate over all files in the Tree
            // you can set Filters to narrow down the results if needed
            try (TreeWalk treeWalk = new TreeWalk(existingRepo)) {
                treeWalk.addTree(tree);
                // not walk the tree recursively so we only get the elements in the top-level directory
                // otherwise set it to true
                treeWalk.setRecursive(true);

                while (treeWalk.next()) {
//                    System.out.println("found: " + treeWalk.getPathString());
                    countFiles++;
                    
			        try ( FileInputStream input = new FileInputStream(treeWalk.getPathString())) {
			        	Integer lines = IOUtils.readLines(input, "UTF-8").size();
			            currentLines += lines;
			            filesToSize.put(treeWalk.getPathString(), lines);
			        }

			        //System.out.println("current sum of lines " + currentLines + " ,lines in file " + treeWalk.getPathString());
                }
//                System.out.println("found: " + countFiles + " files in total");
 //               System.out.println("current sum of lines " + currentLines);
            }
		}

        //Simple snippet which shows how to list all Branches in a Git repository
//        System.out.println("Listing local branches:");
        try (Git git = new Git(existingRepo)) {
            List<Ref> branches = git.branchList().call();
            HashMap<String, Integer> tempBranchesToCommits;
            for (Ref ref : branches) {
            	countBranches++;
            	branchList.add(ref.getName());
//                System.out.println("Branch: " + ref + " " + ref.getName() + " " + ref.getObjectId().getName());
            	branchesToCommits.put(ref.getName(), 0);
            	// a RevWalk allows to walk over commits based on some filtering that is defined
			    //getting all commits in a brach
			    try (RevWalk walk = new RevWalk(existingRepo)) {
			        RevCommit commit = walk.parseCommit(ref.getObjectId());
			      //  System.out.println("Start-Commit: " + commit);

			       // System.out.println("Walking all commits starting at " + ref.getName());
			        walk.markStart(commit);
					
			        for (RevCommit rev : walk) {
			            //System.out.println("Commit: " + rev);

						branchesToCommits.put(ref.getName(), branchesToCommits.get(ref.getName()) + 1);
						countBranches++;

						authorName = rev.getAuthorIdent().getName();
						//mapping how many commits an author has contributed in a branch 
						//if author is first time found in a branch
						
						if( !authorToBranchesToCommits.containsKey(authorName) ){
							tempBranchesToCommits = new HashMap<String, Integer>();
							tempBranchesToCommits.put(ref.getName(), 1);
							authorToBranchesToCommits.put(authorName, tempBranchesToCommits);
			            } else{
			            	//increase the times of commits in the branch and put it into...
			            	tempBranchesToCommits = authorToBranchesToCommits.get(authorName);
			            	if(tempBranchesToCommits.containsKey(ref.getName()))
			            		tempBranchesToCommits.put(ref.getName(), tempBranchesToCommits.get(ref.getName()) + 1);
			            	else
			            		tempBranchesToCommits.put(ref.getName(), 1);
			            }

			            if( !authorToBranches.containsKey(authorName) ){
			            	templist = new ArrayList<String>();
			            	templist.add(ref.getName());
			            	authorToBranches.put(authorName, templist);
			            }
			            else{
			            	templist = authorToBranches.get(authorName);
			            	templist.add(ref.getName());
			            }
			        }

					for (Map.Entry<String, Integer> entry : branchesToCommits.entrySet()) {
					    String key = entry.getKey().toString();
					    Integer value = entry.getValue();
					   // System.out.println("key, " + key + " //value " + value);
					}

			        walk.dispose();
				}
            }


        }
        
        //count tags
        try (Git git = new Git(existingRepo)) {
            List<Ref> call = git.tagList().call();
            for (Ref ref : call) {
               // System.out.println("Tag: " + ref + " " + ref.getName() + " " + ref.getObjectId().getName());
                countTags++;
                // fetch all commits for this tag
                LogCommand log = git.log();

                Ref peeledRef = existingRepo.peel(ref);
                if(peeledRef.getPeeledObjectId() != null) {
                	log.add(peeledRef.getPeeledObjectId());
                } else {
                	log.add(ref.getObjectId());
                }

    			Iterable<RevCommit> logs = log.call();
    			for (RevCommit rev : logs) {
    			//	System.out.println("Commit: " + rev /* + ", name: " + rev.getName() + ", id: " + rev.getId().getName() */);
    			}
            }
        }

      //  System.out.println("countCommits: " + countCommits);
        // Iterate over all authors, using the keySet method, calculate the average of commits of that user
        double d;
 		DecimalFormat df = new DecimalFormat("0.00");

 		//System.out.println("Number of commits and average of every author");
		for (Map.Entry<String, Double> entry : authorToCommits.entrySet()) {
		    String key = entry.getKey().toString();
		    Double value = entry.getValue();
		   // System.out.println("Author key = " + key + " | value = " + value);
		    d = (double)100 * value / countCommits;
		   // System.out.println("   average = " + df.format(d) + "%");
		}

		//System.out.println("Number of commits and average in every branch");
		for (Map.Entry<String, Integer> entry : branchesToCommits.entrySet()) {
		    String key = entry.getKey().toString();
		    Integer value = entry.getValue();
		  //  System.out.println("Branch key = " + key + " | value = " + value);
		    d = (double)100 * value / countBranches;
		   // System.out.println("   average = " + df.format(d) + "%");
		}

		//System.out.println("Average of commits in every branch of every author");
		for (Map.Entry<String, Integer> entry : branchesToCommits.entrySet()) {
			String nameofBranch = entry.getKey().toString();
		    Integer numberOfCommitsInBranch = entry.getValue();
		    for (HashMap.Entry<String, HashMap<String, Integer>> entry1 : authorToBranchesToCommits.entrySet()) {
		    	String nameofAuthor = entry1.getKey().toString();
		    	HashMap<String, Integer> branchNameAndCommits = entry1.getValue();
		    	//if the branch is equal to the branch in which an author has contributed
		    	//System.out.println("branchname: " + branchName + "nameofBranch: " + nameofBranch);
		    	if(branchNameAndCommits.containsKey(nameofBranch)){
		    		d = (double)100 * branchNameAndCommits.get(nameofBranch)/numberOfCommitsInBranch;
		    	//	System.out.println("In branch " + nameofBranch + " author " + nameofAuthor + " has contributed " + df.format(d) + "%");
		    	}
		    }
		}

	//	System.out.println("Average of commits of every author every day, week and month");
		//first and last dates of commits in whole repo
		Date firstDateRepo = new Date(), lastDateRepo = new Date();

		try (Git git = new Git(existingRepo)) {
			commits = git.log().all().call();
			Boolean flag = false;
			for(RevCommit commit: commits){
				PersonIdent authorIdent = commit.getAuthorIdent();
				Date authorDate = authorIdent.getWhen();
				if(!flag){
					firstDateRepo = new Date(authorDate.getTime());
					lastDateRepo = new Date(authorDate.getTime());
					flag = true;
				}
				if(authorDate.before(firstDateRepo)){
					firstDateRepo = new Date(authorDate.getTime());
				}
				if(authorDate.after(lastDateRepo) || authorDate.equals(lastDateRepo)){
					lastDateRepo = new Date(authorDate.getTime());
				}
			}
		}

		DateTime dateTimeFirstRepo = new DateTime(firstDateRepo.getTime());
		DateTime dateTimeLastRepo =  new DateTime(lastDateRepo.getTime());
		ArrayList<Double> tmplist;
		for(String author: authorList){
				Integer alldays = Days.daysBetween(dateTimeFirstRepo, dateTimeLastRepo).getDays();
				Integer allweeks = alldays / 7;
				Integer allmonths = alldays / 30;
				if(alldays == 0) alldays = 1;
				if(allweeks == 0) allweeks = 1;
				if(allmonths == 0) allmonths = 1;
				double comPerDay = (double)authorToCommits.get(author) / (double)alldays;
				double comPerWeek = (double)authorToCommits.get(author) / (double)allweeks;
				double comPerMonth = (double)authorToCommits.get(author) / (double)allmonths;

            	tmplist = new ArrayList<Double>();
            	tmplist.add(comPerDay);
            	tmplist.add(comPerWeek);
            	tmplist.add(comPerMonth);
            	authorToAvgDMY.put(author, tmplist);

			//	System.out.println("Author " + author + " has average of " + comPerDay + " per day, " + comPerWeek + " per week, " + comPerMonth + " per month.");
		}

		//part g
		// Obtain tree iterators to traverse the tree of the old/new commit
		RevObject oldCommit = null, newCommit = null;
		Integer sumOfLines = 0;
		try (Git git = new Git(existingRepo)) {
			Boolean flag = false;
			ObjectReader reader = git.getRepository().newObjectReader(); 
			commits = git.log().all().call();
			
			for(RevCommit commit: commits){
				if(flag){
					CanonicalTreeParser oldTreeIter = new CanonicalTreeParser();
					oldTreeIter.reset( reader, ((RevCommit) oldCommit).getTree() );
					CanonicalTreeParser newTreeIter = new CanonicalTreeParser();
					newTreeIter.reset( reader, commit.getTree() );
					authorName = commit.getAuthorIdent().getName();
					//System.out.println("authorname "+authorName);
					
					// Use a DiffFormatter to compare new and old tree and return a list of changes
					DiffFormatter diffFormatter = new DiffFormatter( DisabledOutputStream.INSTANCE );
					diffFormatter.setRepository( git.getRepository() );
					diffFormatter.setContext( 0 );
					List<DiffEntry> entries = diffFormatter.scan( newTreeIter, oldTreeIter );
				
					// Print the contents of the DiffEntries
					for( DiffEntry entry : entries ) {
						//System.out.println( entry );
						//System.out.println("getoldpath " + entry.getOldPath() );
						//System.out.println("getnewpath " + entry.getNewPath() );
						//System.out.println("getChangeType "+entry.getChangeType());
						//newpath is also the name of the file
						String newpath = entry.getNewPath().toString();
						String type = entry.getChangeType().toString();

						FileHeader fileHeader = diffFormatter.toFileHeader( entry );
						List<? extends HunkHeader> hunks = fileHeader.getHunks();
						for( HunkHeader hunk : hunks ) {
					    	//System.out.println( hunk );
					    	//System.out.println("getOldImage" + hunk.getOldImage() );
					    	//System.out.println("getStartOffset "+ hunk.getStartOffset() );
					    	//System.out.println("getEndOffset "+ hunk.getEndOffset() );
					    	//System.out.println("getNewLineCount "+ hunk.getNewLineCount() );
					    	

					    	if(!authorToLines.containsKey(authorName)){
					    		if(type.equals("ADD")){
					    			if(filesToSize.containsKey(newpath))
					    				authorToLines.put(authorName, filesToSize.get(newpath));
					    			else
					    				authorToLines.put(authorName, 0);
					    		} else{
					    			if(filesToSize.containsKey(newpath))
					    				authorToLines.put(authorName, hunk.getNewLineCount());
					    			else
					    				authorToLines.put(authorName, 0);		
					    		}
					    		
					    	} else{
					    		if(type.equals("ADD")){
					    			if(filesToSize.containsKey(newpath))
					    				authorToLines.put(authorName, authorToLines.get(authorName) + filesToSize.get(newpath));					    			
					    		} else{
					    			if(filesToSize.containsKey(newpath))
					    				authorToLines.put(authorName, authorToLines.get(authorName) + hunk.getNewLineCount());	    			
					    		}
					    	}
					    	sumOfLines += hunk.getNewLineCount();
					  	}
					  	//System.out.println();
					}
				}
				oldCommit = commit;
				flag = true;
			}
		}

		//System.out.println("Average of lines modified by every author");
		for (Map.Entry<String, Integer> entry : authorToLines.entrySet()) {
		    String key = entry.getKey().toString();
		    Integer value = entry.getValue();
		    //System.out.println("Branch key = " + key + " | value = " + value);
		    d = (double)100 * value / currentLines;
		   // System.out.println("   average = " + df.format(d) + "%");
		}

		
		ReportHTML reporter = new ReportHTML(pathReports);
		reporter.createIndex(countFiles, currentLines, countBranches, countTags, authorList.size());
		reporter.reportPage2(reportPage2(pathRepo));
		reporter.reportPage3(authorToCommits, branchesToCommits, authorToBranchesToCommits);
		reporter.reportPage4(authorToAvgDMY);
		reporter.reportPage5(authorToLines);
		System.out.println(">> Report created in the specified location! <<");
	}





	public static HashMap<String, BranchData> reportPage2(String pathtoRepo) throws IOException, GitAPIException, AmbiguousObjectException, MissingObjectException{

		BranchData temp = new BranchData();
		HashMap<String, BranchData> branchMap = new HashMap<String, BranchData>();
		List<String> branchList = new ArrayList<String>();
		Repository existingRepo = new FileRepositoryBuilder()
		    .setGitDir(new File(pathtoRepo))
		    .build();
		
		try (Git git = new Git(existingRepo)) {
        	List<Ref> calls = git.branchList().call();
            for (Ref ref : calls) {
            	String branchName = ref.getName();
            	BranchData bd = new BranchData();
            	List<String> tempList = new ArrayList<String>();
            	int i = 0;
            	Iterable<RevCommit> commits = git.log().add(existingRepo.resolve(branchName)).call(); // check for branchName in resolve might need ObjectId.getName.toString()            	
            	for(RevCommit commit : commits){

            		PersonIdent person = commit.getAuthorIdent();;
            		String id = commit.toString();
            		String message = commit.getFullMessage();
            		String date = person.getWhen().toString();
            		String author = person.getName();

            		String str = git.describe().setTarget(commit.toObjectId()).call();
            		bd.addCommit(id, message, date, author);
            		
            		if(str == null){
            			tempList.add("null");
            		}
            		else{
            			tempList.add(str);
            		}
            		
            		if(i == 0){
            			bd.setLastDate(date);
            		}
            		bd.setCreatDate(date);
            		i = i + 1;

            	}
            	bd.setTagList(tempList);
            	branchMap.put(branchName, bd);
                //System.out.println("Going to next branch!\n");
            }
            return branchMap;
        }
    }

}

