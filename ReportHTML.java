import mypackages.*;

import java.io.File;
import java.io.FileWriter;
import java.io.FilePermission;
import java.util.PropertyPermission;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.util.Collections; 
import java.util.HashMap;
import java.util.Map;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


 public class ReportHTML{


	private String reportPath;
	private File htmlReport;
	private File htmlReport2;
	private File htmlReport3;
	private File htmlReport4;
	private File htmlReport5;
	private File htmlHeader;
	private File htmlFooter;
	private File htmlTemplate;
	private File htmlHeaderPie;


	/* Constructor */
	public ReportHTML(String path){
		reportPath =  path;
		htmlReport =  new File(reportPath + "/" + "ReportPage1.html");
		htmlReport2 = new File(reportPath + "/" + "ReportPage2.html");
		htmlReport3 = new File(reportPath + "/" + "ReportPage3.html");
		htmlReport4 = new File(reportPath + "/" + "ReportPage4.html");
		htmlReport5 = new File(reportPath + "/" + "ReportPage5.html");
		htmlHeader  = new File("header.html");
		htmlFooter = new File("footer.html");
	 	htmlTemplate = new File("index.html");
	 	htmlHeaderPie = new File("piechart_header.html");
	 }
	

	/* Methods */

	/* Creates the first page of the report given the necessary data as int(primitive) */
	public  void createIndex(int files, int lines, int branches, int tags, int commiters){

		String filesString = String.valueOf(files);
		String linesString = String.valueOf(lines);
		String branchesString = String.valueOf(branches);
		String tagsString = String.valueOf(tags);
		String commitersString = String.valueOf(commiters);

//		File htmlTemplate = new File("index.html");  /* index works as template - subject to change */
//		File htmlReport = new File("ReportPage1.html");

		

		try{
		
			String htmlString = FileUtils.readFileToString(htmlTemplate,"UTF8");
			
			htmlString = htmlString.replace("$files", filesString);
			htmlString = htmlString.replace("$lines", linesString);
			htmlString = htmlString.replace("$branches", branchesString);
			htmlString = htmlString.replace("$tags", tagsString);
			htmlString = htmlString.replace("$commiters", commitersString);


			FileUtils.writeStringToFile(htmlReport, htmlString, "UTF8");
	//		System.out.println("Page1 Done!");
		}catch(IOException e){System.out.println(e);}
	}


	/* 2nd Page Generator. Input is:
	   Map<BranchName, BranchData>
	*/
	public  void reportPage2(HashMap<String,BranchData> branchMap){

//		File htmlHeader = new File("header.html");
		//System.out.println("checkpoint1");
//		File htmlFooter = new File("footer.html");
		//System.out.println("checkpoint2");
//		File htmlReport2 = new File("ReportPage2.html");
		//System.out.println("checkpoint3");

		try{

			String htmlString = FileUtils.readFileToString(htmlHeader,"UTF8");
			//System.out.println("checkpoint4");
			FileUtils.writeStringToFile(htmlReport2, htmlString, "UTF8", true);
			//System.out.println("checkpoint5");

			/* write main array */
			String ident = "\n" + "\t" + "\t" + "\t";
			String ident2 = ident + "\t";
			String ident3 = ident2 + "\t";
			String ident4 = ident3 + "\t";

			String temp = ident + "<table class=\"table table-hover\">";
			temp = temp + ident2 + "<thead><tr>";
			temp = temp + ident3 + "<th>Branch</th>" + "<th>Creation Date</th>" + "<th>Last Modification Date</th>";
			temp = temp + ident2 + "</thead></tr>";
			temp = temp + ident2 + "<tbody>";
			FileUtils.writeStringToFile(htmlReport2, temp, "UTF8", true);

			for(HashMap.Entry<String,BranchData> entry : branchMap.entrySet()){

				String branchName = entry.getKey();
				BranchData branchData = entry.getValue();
				String creatDate = branchData.getCreatDate();
				String lastDate = branchData.getLastDate();

				String tri = ident3 + "<tr>";
				String s1 = ident4 + "<td>" + "<a href = \"#" + branchName + "\">" + branchName + "</a>" + "</td>";
				String s2 = ident4 + "<td>" + creatDate + "</td>";
				String s3 = ident4 + "<td>" + lastDate + "</td>";
				String trf = ident3 + "</tr>";

				temp = tri + s1 + s2 + s3 + trf;
				FileUtils.writeStringToFile(htmlReport2, temp, "UTF8", true);
			}

			temp = ident2 + "</tbody>";
			temp = temp + ident + "</table>";
			FileUtils.writeStringToFile(htmlReport2, temp, "UTF8", true);

			/* write subarrays */

			for(HashMap.Entry<String,BranchData> entry : branchMap.entrySet()){

				String branchName = entry.getKey();

				temp = ident + "<h3>" + branchName + "</h3>";
				temp = temp + ident + "<div class=\"well\">" + "<table class=\"table table-bordered align-baseline\" id =\"" + branchName + "\">";
				temp = temp + ident2 + "<thead><tr>";
				temp = temp + ident3 + "<th>Commit ID</th>" + "<th>Commit Message</th>" + "<th>Date</th>" + "<th>Commiter</th>" + "<th>Tag</th>";
				temp = temp + ident2 + "</thead></tr>";
				temp = temp + ident2 + "<tbody>";
				FileUtils.writeStringToFile(htmlReport2, temp, "UTF8", true);

				BranchData branchData = entry.getValue();
				
				List<String> tags = branchData.getTags();
				List<CommitInfo> commits = branchData.log();
				
				int i = 0;
				//System.out.println("tags " + tags.size());
				//System.out.println("commits " + commits.size());
				for(CommitInfo commit : commits){	// not sure parallel list traversal is right

					String id = commit.getId();
					String message = commit.getMessage();
					String date = commit.getDate();
					String commiter = commit.getAuthor();
					//String tag = tags.get(i);
					String tag;
					if(i >= tags.size()){
						tag = "-";
					}
					tag = tags.get(i);
					if(tag == null || tag == "null"){
						tag = "-";
					}


					String tri = ident3 + "<tr>";
					String s1 = ident4 + "<td>" + id + "</td>";
					String s2 = ident4 + "<td>" + message + "</td>";
					String s3 = ident4 + "<td>" +  date + "</td>";
					String s4 = ident4 + "<td>" + commiter  + "</td>";
					String s5 = ident4 + "<td>" + tag + "</td>";
					String trf = ident3 + "</tr>";

					temp = tri + s1 + s2 + s3 + s4 + s5 + trf;
					FileUtils.writeStringToFile(htmlReport2, temp, "UTF8", true);
					i = i + 1;

				}
				temp = ident2 + "</tbody>";
				temp = temp + ident + "</table>" + "</div>";
				FileUtils.writeStringToFile(htmlReport2, temp, "UTF8", true);

			}



			htmlString = FileUtils.readFileToString(htmlFooter,"UTF8");
			FileUtils.writeStringToFile(htmlReport2, htmlString, "UTF8", true);
		}catch(IOException etwas){System.out.println(etwas);}
		//System.out.println("Page2 Done!");

	}



	/*  3rd Page Generator. Inputs are:
		Map<AuthorName, AuthorCommits>,
		Map<BranchName, BranchCommits>,
		Map<BranchName, Map<AuthorName, AuthorBranchCommits>
	*/
	public  void reportPage3(Map<String,Double> authorToCommits, Map<String,Integer> branchesToCommits, HashMap<String, HashMap<String, Integer>> authorToBranchesToCommits){

//		File htmlHeaderPie = new File("piechart_header.html");
//		File htmlFooter = new File("footer.html");
//		File htmlReport3 = new File("ReportPage3.html");

		try{
			/* Setting up and writing the Header - both pie charts included */
			String htmlString = FileUtils.readFileToString(htmlHeaderPie,"UTF8");

			String ident = "\n\t\t\t";
			String ident2 = ident + "\t";
			String ident3 = ident2 + "\t";
			String ident4 = ident3 + "\t";
			String temp;

			String authors = "";
			for(Map.Entry<String,Double> entry : authorToCommits.entrySet()){

				String authorName = entry.getKey();
				String val = entry.getValue().toString();
				temp = ident + "['" + authorName + "'," + val + "],";
				authors = authors + temp;
			}
			htmlString = htmlString.replace("$authorcommits", authors);

			String branches = "";
			for(Map.Entry<String,Integer> entry : branchesToCommits.entrySet()){

				String branchName = entry.getKey();
				String val = entry.getValue().toString();
				temp = ident + "['" + branchName + "'," + val + "],";
				branches = branches + temp;
			}


			htmlString = htmlString.replace("$branchCommits", branches);
			FileUtils.writeStringToFile(htmlReport3, htmlString, "UTF8", true);

			for(HashMap.Entry<String, HashMap<String, Integer>> entry : authorToBranchesToCommits.entrySet()){
				
				String branchName = entry.getKey();
				temp = ident + "<h3>" + branchName + "</h3>";
				temp = temp + ident + "<div class=\"well\">" + "<table class=\"table table-bordered \">";
				temp = temp + ident2 + "<thead><tr>";
				temp = temp + ident3 + "<th>Author</th>" + "<th>Commits Percentage</th>";
				temp = temp + ident2 + "</thead></tr>";
				temp = temp + ident2 + "<tbody>";
				FileUtils.writeStringToFile(htmlReport3, temp, "UTF8", true);

				for(HashMap.Entry<String,Integer> iEntry : entry.getValue().entrySet()){

					String authorName = iEntry.getKey();
					String value = iEntry.getValue().toString();
					String tria = ident3 + "<tr>";
					String s1 = ident4 + "<td>" + authorName + "</td>";
					String s2 = ident4 + "<td>" + value  + "</td>";
					String trfa = ident3 + "</tr>";
					temp = tria + s1 + s2 + trfa;
					FileUtils.writeStringToFile(htmlReport3, temp, "UTF8", true);
				}
				temp = temp + ident2 + "</tbody>";
				temp = temp + ident + "</table>" + "</div>";
				FileUtils.writeStringToFile(htmlReport3, temp, "UTF8", true);
			}
			htmlString = FileUtils.readFileToString(htmlFooter,"UTF8");
			FileUtils.writeStringToFile(htmlReport3, htmlString, "UTF8", true);
		}catch(IOException ioex){System.out.println(ioex);}
	//	System.out.println("ReportPage3 Done!");
	}


	/*  4th Page Generator. Inputs are:
			Map<AuthorName, Integer[3]>
	*/

	public  void reportPage4(Map<String, ArrayList<Double>> authorToTime){

//		File htmlHeader = new File("header.html");
//		File htmlFooter = new File("footer.html");
//		File htmlReport4 = new File("ReportPage4.html");
		try{
			/* Setting up and writing the Header - both pie charts included */
			//System.out.println("Checkpoint 1\n");
			String htmlString = FileUtils.readFileToString(htmlHeader,"UTF8");
			//System.out.println("Checkpoint 2\n");
			FileUtils.writeStringToFile(htmlReport4, htmlString, "UTF8", true);
			//System.out.println("Checkpoint 3\n");

			String ident = "\n\t\t\t";
			String ident2 = ident + "\t";
			String ident3 = ident2 + "\t";
			String ident4 = ident3 + "\t";
			String temp;
			String tri;
			String s1;
			String s2;
			String s3;
			String trf;
			//System.out.println("Checkpoint 4\n");
			for(Map.Entry<String, ArrayList<Double>> entry : authorToTime.entrySet()){


				String authorName = entry.getKey();
				ArrayList<Double> data = entry.getValue();
				String day = data.get(0).toString();
				String month = data.get(1).toString();
				String year = data.get(2).toString();

				temp = ident + "<h3>" + authorName+ "</h3>";
				temp = temp + ident + "<div class=\"well\">" + "<table class=\"table table-bordered \">";
				temp = temp + ident2 + "<thead><tr>";
				temp = temp + ident3 + "<th>Day</th>" + "<th>Week</th>" + "<th>Month</th>";
				temp = temp + ident2 + "</thead></tr>";
				temp = temp + ident2 + "<tbody>";
				tri = ident3 + "<tr>";
				s1 = ident4 + "<td>" + day + "</td>";
				s2 = ident4 + "<td>" + month  + "</td>";
				s3 = ident4 + "<td>" + year  + "</td>";
				trf = ident3 + "</tr>";

				temp = temp + tri + s1 + s2 + s3 + trf;
				temp = temp + ident2 + "</tbody>";
				temp = temp + ident + "</table>" + "</div>";
				FileUtils.writeStringToFile(htmlReport4, temp, "UTF8", true);
			}
		}catch(IOException ioex3){System.out.println(ioex3);}
		//System.out.println("ReportPage4 Done!");
	}


	/*  5th Page Generator. Inputs are:
			Map<AuthorName, Integer>
	*/
	public  void reportPage5(Map<String,Integer> authorToLines){

//		File htmlHeader = new File("header.html");
//		File htmlFooter = new File("footer.html");
//		File htmlReport5 = new File("ReportPage5.html");
		try{
			/* Setting up and writing the Header - both pie charts included */
			String htmlString = FileUtils.readFileToString(htmlHeader,"UTF8");
			FileUtils.writeStringToFile(htmlReport5, htmlString, "UTF8", true);

			String ident = "\n\t\t\t";
			String ident2 = ident + "\t";
			String ident3 = ident2 + "\t";
			String ident4 = ident3 + "\t";
			String temp;
			String tri;
			String s1;
			String trf;

			for(Map.Entry<String,Integer> entry : authorToLines.entrySet()){

				String authorName = entry.getKey();
				String lines = entry.getValue().toString();

				temp = ident + "<h3>" + authorName + "</h3>";
				temp = temp + ident + "<div class=\"well\">" + "<table class=\"table table-bordered \">";
				temp = temp + ident2 + "<thead><tr>";
				temp = temp + ident3 + "<th># Lines Modified</th>";
				temp = temp + ident2 + "</thead></tr>";
				temp = temp + ident2 + "<tbody>";
				tri = ident3 + "<tr>";
				s1 = ident4 + "<td>" + lines + "</td>";
				trf = ident3 + "</tr>";

				temp = temp + tri + s1 + trf;
				temp = temp + ident2 + "</tbody>";
				temp = temp + ident + "</table>" + "</div>";
				FileUtils.writeStringToFile(htmlReport5, temp, "UTF8", true);
			}
		}catch(IOException ioex31){System.out.println(ioex31);}
		//System.out.println("ReportPage5 Done!");
	}

};










