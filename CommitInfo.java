package mypackages;


public class CommitInfo{
	
	String id;
	String message;
	String date;
	String author;

	public CommitInfo(String i, String m, String d, String a){
		id = i;
		message = m;
		date = d;
		author = a;
	}

	public String getId(){
		return id;
	}

	public String getMessage(){
		return message;
	}

	public String getDate(){
		return date;
	}

	public String getAuthor(){
		return author;
	}
}