Gia tin paragogi ton html anaforon exoyn anaptyxthei oi klaseis CommitInfo, BranchData, ReportHTML.
Kapoio instance tis ReportHTML arxikopoieitai me to path sto opoio prepei na dimiourgithoun oi anafores
to opoio de tha prepei austira na teleionei me ton xaraktira "/", px "./somepath" anti gia  "./somepath/".
Epeidi paragontai 5 html arxeia, gia tin kaluteri diaxeirisi exoun anaptyx8ei kapoies html selides
pou xrisimopoiountai san templates. Mesa apo ton kodika java, diabazetai to template san string kai ginontai
replace substrings tis morfis $someVariable me html pou xtizetai on the fly. Enallaktika, diabazetai
kai grafetai to header template se kapoio html report, ginetai apend html on the fly kai epeita append
to footer template. To executable tha prepei na trexei sto idio directory me ta antistoixa .html template files.
Exei xrisimopoiithei i bibliothiki bootstrap kai kapoio google pie chart. Ta dependencies antimetopizontai
meso CDN, gi auto krinetai anagkaia i sundesi sto diadiktuo prokeimenou oles oi selides anaforon kai eidikotera i
ReportPage3 na emfanizei ta aparaitia stoixeia.


Akolou8ei to idio minima se ellinikous xarakthres.

Για την παραγωγη των html αναφορων εχουν αναπτυχθει οι κλασεις CommitInfo, BranchData ReportHTML.
Καθε στιγμιοτυπο της ReportHTML αρχεικοποιε´ιται με το path στο οποιο πρεπει να δημιουργηθουν οι αναφορες,
το οποιο δε θα πρεπει αυστηρα να τελειωνει με τον χαρκτηρα "/", πχ "./somepath" αντι για "./somepath/".
Επειδη παραγονται 5 html ααρχεια, για την καλυτερη διαχειριση εχουν αναπτυχθει καποια html αρχεια που
χρησιμοποιουνται σαν templates. Μεσα απο τον java κωδικα, διαβαζεται το template σαν String και γινονται
replace substrings της μορφης $someVariable με html που χτιζεται on the fly. Εναλλτκικα, διαβαζεται και γραφεται
το header template, γινεται append html που χτιζεται on the fly κι επειτα append footer template. Το εκτελεσιμο
θα πρεπει να τρεχει απο το ιδιο directory με τα αντιστοιχα .html template αρχεια.
Εχει χρησιμοποιηθει η βιβλιοθηκη boostrap και καποιο google piechart. Τα dependencies αντιμετωπιζονται μεσω
CDN, γι αυτο κρινεται αναγκαια η συνδεση στο διαδικτυο προκειμενου ολες οι σελιδες αναφορων και ειδικοτερα 
η ReportPage3.html να εμφανιζουν τα απαραιτητα στοιχεια.


1115201000066 - Δημητριου Χρηστος 
1115201100097 - Λιακοπουλος Βασιλειος.