package mypackages;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class BranchData{

	String creatDate;
	String lastDate;
	List<CommitInfo> commits;
	List<String> tags;

	public BranchData(){
		commits = new ArrayList<CommitInfo>();
		tags = new ArrayList<String>();
	}

	public String getCreatDate(){
		return creatDate;
	}

	public String getLastDate(){
		return lastDate;
	}

	public List<CommitInfo> log(){
		return commits;
	}

	public List<String> getTags(){
		return tags;
	}

	public void setCreatDate(String cd){
		creatDate = cd;
	}

	public void setLastDate(String ld){
		lastDate = ld;
	}

	public void addCommit(String i, String m, String d, String a){
		CommitInfo c = new CommitInfo(i,m,d,a);
		commits.add(c);
	}

	public void addCommit(CommitInfo c){
		commits.add(c);
	}
	public void addTag(String t){
		tags.add(t);
	}

	public void setTagList(List<String> l){
		tags = l;
	}
}