compile: javac -cp \* Main.java ReportHTML.java

run: java -cp ./org.eclipse.jgit-4.6.1.201703071140-r.jar:./commons-io-2.5.jar:./slf4j-api-1.7.24.jar:./slf4j-nop-1.7.24.jar:./joda-time-2.4.jar:. Main /home/chris/Desktop/SoftTech_localrepo /home/chris/Desktop/SoftTech_localrepo

To make the final jar:(after you create it you can run it passing the 2 arguments, path to repo, path to reports)
jar cmf Manifest.txt app.jar Main.class org.eclipse.jgit-4.6.1.201703071140-r.jar commons-io-2.5.jar slf4j-api-1.7.24.jar slf4j-nop-1.7.24.jar joda-time-2.4.jar

run the final jar:
java -jar app.jar /home/chris/Desktop/SoftTech_localrepo /home/chris/Desktop/SoftTech_localrepo
